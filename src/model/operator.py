from abstract_operator import AbstractOperator 
from abc import ABC
from devopsX import factories

class BaseMSDevOpsOperator(AbstractOperator,ABC):

    def __init__(self,collection_name:str, organization_url:str, personal_access_token:str):
        super().__init__(collection_name=collection_name)
        
        self.organization_url = organization_url
        self.personal_access_token = personal_access_token
        self.service = None    
        
    def execute(self):
        result = self.service.get_all()
        self.save(result) 
   

class MSDevOpsProjectOperator(BaseMSDevOpsOperator):

    def __init__ (self,organization_url, personal_access_token):
        super(MSDevOpsProjectOperator,self).__init__(collection_name="project",personal_access_token=personal_access_token,organization_url=organization_url)
        self.service = factories.ProjectFactory(personal_access_token=personal_access_token,organization_url=self.organization_url)

         
class MSDevOpsTeamOperator(BaseMSDevOpsOperator):

    def __init__ (self,organization_url, personal_access_token):
        super(MSDevOpsTeamOperator,self).__init__(collection_name="team",personal_access_token=personal_access_token,organization_url=organization_url)
        self.service = factories.TeamFactory(personal_access_token=self.personal_access_token,organization_url=self.organization_url)

class MSDevOpsInteractionOperator(BaseMSDevOpsOperator):

    def __init__ (self, organization_url, personal_access_token):
        super(MSDevOpsInteractionOperator,self).__init__(collection_name="interaction",personal_access_token=personal_access_token,organization_url=organization_url)
        self.service = factories.InteractionFactory(personal_access_token=self.personal_access_token,organization_url=self.organization_url)

class MSDevOpsTeamMemberOperator(BaseMSDevOpsOperator):

    def __init__ (self, organization_url, personal_access_token):
        super(MSDevOpsTeamMemberOperator,self).__init__(collection_name="teammember",personal_access_token=personal_access_token,organization_url=organization_url)
        self.service = factories.TeamMemberFactory(personal_access_token=self.personal_access_token,organization_url=self.organization_url)

class MSDevOpsWorkitemOperator(BaseMSDevOpsOperator):

    def __init__ (self, organization_url, personal_access_token):
        super(MSDevOpsWorkitemOperator,self).__init__(collection_name="workitem",personal_access_token=personal_access_token,organization_url=organization_url)
        self.service = factories.WorkItemFactory(personal_access_token=self.personal_access_token,organization_url=self.organization_url)