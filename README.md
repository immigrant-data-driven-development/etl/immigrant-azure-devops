# Immigrant: ETL

## 🚀 Goal

Retrieve data from application (e.g., Azure DevOps, Gitlab, and Sonar) that support software development process and save in a Mongo Database. 

Immigrant ETL uses [airflow](https://airflow.apache.org/) to perform the ETL processes. 

## ⚙️ Pre-Requirement

The following programs needs to be instaled:

1. Docker
2. Docker Compose
3. Mongo Database

## 🔧 Install 

Download the repository with the following command:

```bash
git clone https://gitlab.com/immigrant-data-driven-development/etl/immigrant-etl.git
```

Run the following command to install all components:

```bash
docker compose up -d 
```

1. Docker
2. Docker Compose
3. Mongo Database


## 💻 Usage and Configuration

Access the [http://localhost:8080](http://localhost:8080).

### Configuration a Mongo Connection

1. Click in 
2. 

### Configure Application variables:


## 🛠️ Build in

- [Airflow](https://airflow.apache.org/):   Airflow is a platform created by the community to programmatically author, schedule and monitor workflows. 
- [Docker Compose](https://docs.docker.com/compose/): Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration.
- [Mongo Database](https://www.mongodb.com/): A noSQL database.


## 📕 Reference
